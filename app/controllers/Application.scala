package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import models._
import org.joda.time.{DateTime, Period}

object Application extends Controller {

  def index = Action {
    Redirect(routes.Application.display)
  }

  def display = Action {
    Ok(views.html.index(getEvent))
  }
  
  def addEventPage = Action {
    Ok(views.html.addEvent(eventForm))
  }
  
  def discover = Action {
    Ok(views.html.discover())
  }
  
  def getEvent() = {
    if(EventSimple.selectPath().size > 0 && EventSimple.selectPath()(0).title != null &&  
        !EventSimple.selectPath()(0).title.isEmpty()){
      val event = EventSimple.selectPath()(0)
      (event.title, event.main_image_url, event.city, event.address, event.date_event, event.description)
    }
    else
    	("Manu Chao in concert", "http://www.concertionline.com/wp-content/gallery/manu-chao-a-bologna/dsc_4370a.jpg", "Marina di Pisa", "Piazza Centrale, 1", null, null)
  }

  def newEvent() = Action { implicit request =>
    pathForm.bindFromRequest.fold(
      errors => BadRequest(views.html.index(getEvent)),
      path => {
        val (path, title, descriptions, city, address, date_event, description) 
        	= eventForm.bindFromRequest.get
        EventSimple.create(path,title, city, address, date_event, description)
        Redirect(routes.Application.display)
      })
  }

  def deleteEvents = Action {
    	EventSimple.deleteEvents
    Redirect(routes.Application.display)
  }

  val taskForm = Form(
    "label" -> nonEmptyText)

  val pathForm = Form(
    "path" -> nonEmptyText) 
    
  val eventForm = Form(
  		tuple(
  				"path" -> text,
  				"title" -> text,
  				"description" -> text,
  				"city" -> text,
  				"address" -> text,
  				"date_event" -> date,
  				"description" -> text
 )
)

}