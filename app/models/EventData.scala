package models

import org.joda.time.{DateTime, Period}

 case class EventData(path: String, title: String, time: DateTime, description: String) 