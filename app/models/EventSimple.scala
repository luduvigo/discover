package models

import anorm._
import anorm.SqlParser._
import play.api.db._
import play.api.Play.current

//Here I declare a Task, 
//going to be a long way to the Discover application

case class EventSimple(id: Long, main_image_url: String, title: String, city: String, 
      address: String, date_event: java.util.Date, description: String)

object EventSimple {
  
  val event = {
	get[Long]("id") ~
	get[String]("main_image_url")  ~
	get[String]("title")  ~
	get[String]("city")  ~
	get[String]("address")  ~
	get[java.util.Date]("date_event")  ~
	get[String]("description") map {
	  case id~main_image_url~title~city~address~date_event~description 
	  	=> EventSimple(id,main_image_url,title,city,address,date_event,description)
	}
  }
  
  def selectPath(): List[EventSimple] = DB.withConnection { implicit c =>
   val res = SQL("SELECT * FROM event WHERE id >= RANDOM() * (SELECT MAX(id) FROM event) LIMIT 1"
    		).as(event *)
   res
  }
  
  def create(main_image_url: String, title: String, city: String, 
      address: String, date_event: java.util.Date, description: String) {
    DB.withConnection { implicit c =>
      SQL("""insert into event (main_image_url, title, city, address, date_event, description) 
      		values ({main_image_url},{title},{city},{address},{date_event},{description})"""
          ).on(
    		  'main_image_url -> main_image_url,
    		  'title -> title,
    		  'city -> city,
    		  'address -> address,
    		  'date_event -> date_event,
    		  'description -> description
      ).executeUpdate()
    }
  }
  
  def deleteEvents() {
    	DB.withConnection { implicit c =>
      	SQL("delete from event").executeUpdate()
    	}
  }
}

