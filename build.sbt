name := """discover"""

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  javaJpa,
	"org.hibernate" % "hibernate-entitymanager" % "4.3.4.Final",
	"postgresql" % "postgresql" % "9.1-901-1.jdbc4"
)

// The Typesafe repository
resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
