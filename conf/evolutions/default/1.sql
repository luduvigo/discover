# --- Created by Ebean DDL

# --- !Ups

CREATE SEQUENCE event_id_seq;
CREATE SEQUENCE users_id_seq;
CREATE SEQUENCE images_id_seq;
CREATE SEQUENCE going_id_seq;
CREATE SEQUENCE category_id_seq;
CREATE SEQUENCE comments_id_seq;

CREATE TABLE event (
	id integer NOT NULL DEFAULT nextval('event_id_seq'),
	url varchar(2000),
	main_image_url varchar(2000),
	title varchar(511),
	description text,
	start_time date,
	stop_time date,
	all_day bool default false,
	venue_name varchar(255),
	venue_type varchar(255),
	address varchar(511),
	city varchar(50),
	region varchar(50),
	postalcode varchar(10),
	country varchar(30),
	latitude int8,
	longitude int8,
	free bool,
	price decimal(10,2),
	visits_number integer 
);

CREATE TABLE users (
	user_id integer NOT NULL DEFAULT nextval('users_id_seq'),
	user_name varchar(50),
	user_email varchar(255),
	user_password_salt varchar(2000),
	user_password varchar(16),
	user_creation_date date
);

CREATE TABLE images (
	image_id integer NOT NULL DEFAULT nextval('images_id_seq'),
	image_url varchar(2000),
	image_width integer,
	image_height integer,
	image_caption varchar(500)
);

CREATE TABLE going (
	going_id integer NOT NULL DEFAULT nextval('going_id_seq'),
	going_user integer,
	going_event integer
);

CREATE TABLE category (
	category_id integer NOT NULL DEFAULT nextval('category_id_seq'),
	category_name varchar(50),
	category_event integer
);

CREATE TABLE comments (
	comment_id integer NOT NULL DEFAULT nextval('comments_id_seq'),
	comment_user integer,
	comment_event integer
);

# --- !Downs

DROP TABLE event;
DROP SEQUENCE event_id_seq;

DROP TABLE users;
DROP SEQUENCE users_id_seq;

DROP TABLE images;
DROP SEQUENCE images_id_seq;

DROP TABLE going;
DROP SEQUENCE going_id_seq;

DROP TABLE category;
DROP SEQUENCE category_id_seq;

DROP TABLE comments;
DROP SEQUENCE comments_id_seq;
